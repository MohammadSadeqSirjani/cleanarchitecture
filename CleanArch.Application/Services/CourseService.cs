﻿using CleanArch.Application.Interfaces;
using CleanArch.Application.ViewModes;
using CleanArch.Domain.Interfaces;
using CleanArch.Domain.Models;
using System.Collections.Generic;
using System.Linq;

namespace CleanArch.Application.Services
{
    public class CourseService : ICourseService
    {
        private readonly ICourseRepository _courseRepository;

        public CourseService(ICourseRepository courseRepository)
        {
            _courseRepository = courseRepository;
        }

        public CourseViewModel GetCourse()
        {
            return new CourseViewModel() 
            {
                Courses = _courseRepository.GetCourses()
            };
        }
    }
}
