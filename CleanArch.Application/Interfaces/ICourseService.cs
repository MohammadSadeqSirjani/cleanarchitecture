﻿using CleanArch.Application.ViewModes;
using System.Collections.Generic;

namespace CleanArch.Application.Interfaces
{
    public interface ICourseService
    {
        CourseViewModel GetCourse(); 
    }
}
