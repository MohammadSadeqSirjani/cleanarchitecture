﻿using CleanArch.Domain.Models;
using System.Collections.Generic;

namespace CleanArch.Application.ViewModes
{
    public class CourseViewModel
    {
        public IEnumerable<Course> Courses { get; set; }
    }
}
