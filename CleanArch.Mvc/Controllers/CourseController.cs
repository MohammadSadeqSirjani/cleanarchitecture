﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CleanArch.Application.Interfaces;
using CleanArch.Domain.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CleanArch.Mvc.Controllers
{
    [Authorize]
    public class CourseController : Controller
    {
        private readonly ICourseService _coureseServices;

        public CourseController(ICourseService coureseServices)
        {
            _coureseServices = coureseServices;
        }

        public IActionResult Index()
        {
            var model = _coureseServices.GetCourse();

            return View(model);
        }
    }
}